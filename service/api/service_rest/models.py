from django.db import models
from django.urls import reverse

# Create your models here.

class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id =models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.first_name
    def get_api_url(self):
        return reverse('list_technicians', kwargs={'id': self.employee_id})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold =models.BooleanField(default=False)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=17, unique=True)
    status = models.CharField(max_length=50)
    customer =models.CharField(max_length=50)
    technician = models.ForeignKey(
       Technician,
       related_name='appointments',
       on_delete=models.CASCADE
    )
    def __str__(self):
        return f'{self.customer} {self.status}'

    def get_api_url(self):
        return reverse ('list_appointments', kwargs={'id': self.id})
