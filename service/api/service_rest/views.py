from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
from django.http import JsonResponse
import json

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties =[
        'vin',
        'sold',
    ]
class TechnicianListEncoder(ModelEncoder):
    model =Technician
    properties =[
        'first_name',
        'last_name',
        'employee_id',
        'id',
    ]
class AppointmentEncoder(ModelEncoder):
    model =Appointment
    properties =[
        'date_time',
        'reason',
        'vin',
        'customer',
        'technician',
        'id',
        'status',
    ]
    encoders = {
        'technician': TechnicianListEncoder(),
    }

@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method=='GET':
        technicians = Technician.objects.all()
        return JsonResponse (
            {'technicians': technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
           new_technician = Technician.objects.create(**content)
           return JsonResponse(
               new_technician,
               encoder=TechnicianListEncoder,
               safe=False,
           )
        except Technician.DoesNotExist:
            return JsonResponse({'message': 'Tech does not exist'},
                                staus =400,
                                )

@require_http_methods(['GET', 'DELETE'])
def show_technician(request, id):
    if request.method =='GET':
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        request.method=='DELETE'
        count, _ =Technician.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})

@require_http_methods(['GET'])
def list_automobiles(request):
    if request.method =='GET':
        automobile =AutomobileVO.objects.all()
        return JsonResponse(
            {'automobiles': automobile},
            encoder=AutomobileVOEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'POST'])
def list_appointments(request):
    if request.method =='GET':
        appointments = Appointment.objects.all()
        return JsonResponse (
            {'appointments': appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        content =json.loads(request.body)
        # try:
        #     new_appointment =Appointment.objects.create(**content)
        #     return JsonResponse(
        #         new_appointment,
        #         encoder=AppointmentEncoder,
        #         safe =False
        #    )
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"error": "Technician does not exist"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

# @require_http_methods(['GET', 'PUT', 'DELETE'])
# def show_appointment_details(request, id):
#     if request.method =='GET':
#         appointment = Appointment.objects.get(id=id)
#         return JsonResponse(
#             appointment,
#             encoder=AppointmentEncoder,
#             safe=False,
#         )
#     elif request.method =='DELETE':
#         count, _ =Appointment.objects.filter(id=id).delete()
#         return JsonResponse({'deleted': count >0})
#     else:
#         content = json.loads(request.body)
#         Appointment.objects.filter(id=id).update(**content)
#         appointment = Appointment.objects.get(id=id)
#         return JsonResponse(
#             appointment,
#             encoder=AppointmentEncoder,
#             safe=False,
#         )
@require_http_methods(['DELETE'])
def delete_appointment(request, id):
    if request.method =='DELETE':
        count, _ =Appointment.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count >0})

@require_http_methods(['PUT'])
def finish_appointment(request, id):
    if request.method =='PUT':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status='Finished'
            appointment.save()
            return JsonResponse(
            {'message': 'Appointment finished successfully'}
        )
        except Appointment.DoesNotExist:
            return JsonResponse(
            {'message': 'Unable to update appointment'},
            staus =400,
        )
@require_http_methods(['PUT'])
def cancel_appointment (request, id):
    if request.method =='PUT':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status='Canceled'
            appointment.save()
            return JsonResponse(
            {'message': 'Appointment cancelled successfully'}
        )
        except Appointment.DoesNotExist:
            return JsonResponse(
            {'message': 'Unable to update appointment'},
            staus =400,
        )
