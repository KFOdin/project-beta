from django.urls import path

from .views import list_appointments, list_automobiles, api_list_technicians, show_technician, delete_appointment, finish_appointment, cancel_appointment

urlpatterns  = [
    path ('technicians/', api_list_technicians, name='api_list_technicians'),
    path('automobiles/',list_automobiles, name='list_automobiles'),
    path('appointments/', list_appointments, name='list_appointments'),
    path('technicians/<int:id>/', show_technician, name='show_technician'),
    path('appointments/<int:id>/', delete_appointment, name='delete_appointment'),
    path('appointments/<int:id>/cancel/', cancel_appointment, name='cancel_appointment'),
    path('appointments/<int:id>/finish/', finish_appointment, name='finish_appointment'),
]
