# Generated by Django 4.0.3 on 2023-09-07 23:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0010_alter_appointment_status_and_more'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Status',
        ),
    ]
