import React, { useState } from "react";

function SalesPeopleForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeID] = useState("");

  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };
  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };
  const handleEmployeeIdChange = (e) => {
    const value = e.target.value;
    setEmployeeID(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const Url = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(Url, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      setEmployeeID("");
    }
  };
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salespeople-form">
              <div className="form-floating mb-3">
                <input
                  value={firstName}
                  onChange={handleFirstNameChange}
                  placeholder="first_name"
                  required
                  type="text"
                  name="first_name"
                  id="first_name"
                  className="form-control"
                />
                <label htmlFor="name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={lastName}
                  onChange={handleLastNameChange}
                  placeholder="last_name"
                  required
                  type="text"
                  name="last_name"
                  id="last_name"
                  className="form-control"
                />
                <label htmlFor="name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={employeeId}
                  onChange={handleEmployeeIdChange}
                  placeholder="employee_id"
                  required
                  type="text"
                  name="employee_id"
                  id="employee_id"
                  className="form-control"
                />
                <label htmlFor="name">Employee ID</label>
              </div>
              <div>
                <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesPeopleForm;
