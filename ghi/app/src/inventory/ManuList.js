import { useEffect, useState } from "react";

function ManuList() {
  const [manufacturers, setManufacturers] = useState([]);

  const getData = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Manufacturers </h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manu) => {
            return (
              <tr key={manu.id}>
                <td>{manu.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ManuList;
