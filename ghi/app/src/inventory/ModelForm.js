import React, { useEffect, useState } from "react";

function ModelForm() {
  const [manufacturers, setManufacturers] = useState([]);
  const [manufacturer, setManufacturer] = useState("");
  const [name, setName] = useState("");
  const [picture_url, setPictureUrl] = useState("");

  const fetchData = async () => {
    const Url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(Url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleManuChange = (e) => {
    const value = e.target.value;
    setManufacturer(value);
  };
  const handleNameChange = (e) => {
    const value = e.target.value;
    setName(value);
  };
  const handlePictureChange = (e) => {
    const value = e.target.value;
    setPictureUrl(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      name: name,
      picture_url: picture_url,
      manufacturer_id: manufacturer,
    };

    const Url = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(Url, fetchConfig);
    if (response.ok) {
      setManufacturer("");
      setName("");
      setPictureUrl("");
    }
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create A New Model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input
                  value={name}
                  onChange={handleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={picture_url}
                  onChange={handlePictureChange}
                  placeholder="Picture Url"
                  required
                  type="url"
                  name="picture_url"
                  id="picture_url"
                  className="form-control"
                />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div>
                <select
                  value={manufacturer}
                  required
                  onChange={handleManuChange}
                  name="manufacturer"
                  id="manufacturer"
                  className="form-select"
                >
                  <option>Choose a Manufacturer</option>
                  {manufacturers.map((manufacturer) => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div>
                <button className="btn btn-primary">Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModelForm;
