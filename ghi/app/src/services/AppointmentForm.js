import React, { useEffect, useState } from "react";

function AppointmentForm () {
    const[dateTime, setDateTime]=useState('');
    const[reason, setReason]=useState('');
    const[vin, setVin]=useState('');
    const[customer, setCustomer]=useState('');
    const[technician, setTechnician]=useState('');
    const[technicians, setTechnicians]=useState([]);

    const handleDateTimeChange=(event)=>{
        const value = event.target.value;
        if (value ===''){
            setDateTime('');
        } else {
            setDateTime(value);
        }
    }
    const handleReasonChange=(event)=>{
        const value = event.target.value;
        setReason(value)
    }
    const handleVinChange=(event)=>{
        const value = event.target.value;
        setVin(value)
    }
    const handleCustomerChange=(event)=>{
        const value = event.target.value;
        setCustomer(value);
    }
    const handleTechnicianChange=(event)=>{
        const value = event.target.value;
        setTechnician(value);
    }

    const handleSubmit = async (event)=>{
        event.preventDefault();
        const data ={
            date_time: dateTime,
            reason: reason,
            vin: vin,
            customer: customer,
            technician: technician,
        };
        const appointmentUrl ='http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (appointmentUrl, fetchConfig);
        if (response.ok){
            const newAppointment = await response.json();
            setDateTime('');
            setReason('');
            setVin('');
            setCustomer('');
            setTechnician('');
            setTechnicians([]);

        }
    }
    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const response = await fetch (technicianUrl)

        if(response.ok){
            const data= await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(()=> {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create An Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment">
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" maxLength={17} name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateTimeChange} value={dateTime} placeholder="dateTime" required type="datetime-local" name="dateTime" id="dateTime" className="form-control"/>
                <label htmlFor="vin">Date/Time</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="mb-3">
              <select onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                <option value="">Technician</option>
                {technicians.map(technician => {
                  return (
                    <option key={ technician.id } value={ technician.id }>{ technician.first_name }{ technician.lastname }</option>
                  )
                })}
              </select>
              </div>
              <button type='submit' className="btn btn-primary">Create</button>
              </form>
          </div>
        </div>
      </div>

    )

}
export default AppointmentForm
