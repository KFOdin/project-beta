import React, { useEffect, useState } from "react";

function AppointmentHistory(props) {
    const [appointments, setAppointments]= useState([]);
    const [query, setQuery] = useState("");
    const[autos, setAutomobiles]= useState([]);

    const loadAppointments=async () =>{
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
        } else {
          console.error(response);
        }
      }

      useEffect(() => {
        loadAppointments();
      }, []);

      const loadAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
          const data = await response.json();
          setAutomobiles(data.autos);
        } else {
          console.error(response);
        }
      }

      useEffect(() => {
        loadAutomobiles();
      }, []);

      return (
        <>
        <h1>Service History</h1>
        <div className="app">
            <input
            className="search"
            placeholder="Search..."
            onChange={(e)=> setQuery(e.target.value)}
            />
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date / Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {appointments.filter((appointment)=> appointment.vin.toLowerCase().includes(query)).map(appointment => {
                const automobile =autos.find(auto=>auto.vin===appointment.vin);
              return (
                <tr key={appointment.vin}>
                  <td>{ appointment.vin }</td>
                  <td>{ automobile ? automobile.sold ? "Yes" : 'No' :"No"}</td>
                  <td>{ appointment.customer }</td>
                  <td>{ appointment.date_time }</td>
                  <td>{ appointment.technician.first_name } {appointment. technician.last_name }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.status }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </>
      );
    }

export default AppointmentHistory;
